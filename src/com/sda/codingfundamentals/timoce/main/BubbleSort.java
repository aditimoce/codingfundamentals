package com.sda.codingfundamentals.timoce.main;

import java.util.Arrays;

public class BubbleSort {

	public static void main(String[] args) {

		int[] myArray = { 3, 5, 7, 4, 6 };
		int[] sortedArray = bubbleSort(myArray);
		System.out.println(Arrays.toString(sortedArray));
	}

	static int[] bubbleSort(int[] array) {
		int[] arraySorted = new int[array.length];

		for (int i = 0; i < array.length; i++) {
			for (int j = 1; j < array.length; j++) {
				if (array[i] > array[j]) {
					int temp = array[i];
					arraySorted[i] = array[j];
					arraySorted[j] = temp;
				}
			}
		}

		return arraySorted;
	}

}
