package com.sda.codingfundamentals.timoce.main;

import java.util.Scanner;

public class TimeConverter {

	public static void main(String[] args) {
		// Write a Java program to convert seconds to hour, minute and seconds.
		// Sample Output:
		// Input seconds: 86399
		// 23:59:59

		Scanner scanner = new Scanner(System.in);

		System.out.print("Insert seconds: ");
		int secondsFromInput = scanner.nextInt();

		int seconds = secondsFromInput;
		int remainingSeconds = seconds % 60;
		int minutes = (seconds - remainingSeconds) / 60;
		int remainingMinutes = ((seconds - remainingSeconds) / 60) % 60;
		int hours = (minutes - remainingMinutes) / 60;

		if (hours < 10) {
			System.out.print("0" + hours);
		} else {
			System.out.print(hours);
		}
		if (remainingMinutes < 10) {
			System.out.print(":0" + remainingMinutes);
		} else {
			System.out.print(":" + remainingMinutes);
		}
		if (remainingSeconds < 10) {
			System.out.print(":0" + remainingSeconds);
		} else {
			System.out.print(":" + remainingSeconds);
		}
		scanner.close();
	}
}
