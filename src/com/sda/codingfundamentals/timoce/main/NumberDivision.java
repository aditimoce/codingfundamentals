package com.sda.codingfundamentals.timoce.main;

public class NumberDivision {

	public static void main(String[] args) {
		// Write a program which will find all numbers which are divisible by 7 but are
		// not a multiple of 5,
		// between 2177 and 3262 (both included).
		// The numbers obtained should be printed in a comma-separated sequence on a
		// single line in descending order.

		int smallNumber = 2177;
		int largeNumber = 3262;

		for (int numberToCheck = largeNumber; numberToCheck >= smallNumber; numberToCheck--) {
			if (numberToCheck % 7 == 0 && numberToCheck % 5 != 0) {
				System.out.print(numberToCheck + ", ");
			}
		}
	}
}
