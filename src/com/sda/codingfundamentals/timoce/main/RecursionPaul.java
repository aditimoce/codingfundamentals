package com.sda.codingfundamentals.timoce.main;

public class RecursionPaul {

	private static int mySum = 0;

	public static void main(String[] args) {

		int mySum = printNumbers(); // 55 -> mySum
		System.out.println(55);
		System.out.println();
		System.out.println(mySum);

		// int suma = printNumbers();
		// printNumbersRecursive(0);

		sumRecursive(0);
//		int finalSum = sumRecursiveWithReturn(0);
//		System.out.println("Sum is : "+ finalSum);
	}

	static int printNumbers() {
		int sum = 0;
		for (int i = 0; i <= 10; i++) {
			sum += i;
			// System.out.print(i + " ");
		}
		return sum;
	}

	static void printNumbersRecursive(int i) {
		if (i == 10) {
			System.out.println(i + " ");
			return;
		}
		System.out.print(i + " ");
		i++;
		printNumbersRecursive(i);
		// TODO: print again
	}

	// with global variable and no return type
	static void sumRecursive(int i) {

		if (i == 10) {
			mySum = mySum + i;
			return;
		}
		mySum = mySum + i;
		sumRecursive(++i); // TODO: ++i vs i++
		// return;
	}

	// with global var and return sum;
	static int sumRecursiveWithReturn(int i, int suma) {
		suma = suma + i;
		if (i == 10) {
			return suma;
		}
		i++;
		return sumRecursiveWithReturn(i, suma); // TODO: ++i vs i++
	}
}