package com.sda.codingfundamentals.timoce.main;

import java.util.Scanner;

public class ProfitInvestment {

	public static void main(String[] args) {
		// Calculate how many years it will take to double your investment with 5%
		// return / year.
		// Extra 1: ask for return from input %, read from console % ;

		Scanner scanner = new Scanner(System.in);

		System.out.print("How much do you want to invest? ");
		int investmentFromInput = scanner.nextInt();

		double initialInvestment = investmentFromInput;
		double targetInvestment = 150;
		double interestRate = 1.05;
		int numberOfYears = 0;

		while (initialInvestment <= targetInvestment) {
			initialInvestment = initialInvestment * interestRate;
			numberOfYears++;
			System.out.println("Current balance after " + numberOfYears + " years" + " is: " + initialInvestment);
		}
		System.out.println("Target Investment is reached in " + numberOfYears + " years.");

		scanner.close();
	}
}
