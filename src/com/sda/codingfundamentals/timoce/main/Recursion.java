package com.sda.codingfundamentals.timoce.main;

public class Recursion {

	public static void main(String[] args) {
		// int sum = printNumbers();
		int sum = printNumbersRecursive(0);

		System.out.println("Sum is: " + sum);
	}

//	static int printNumbers() {
//		int sum = 0;
//		for (int i = 0; i <= 10; i++) {
//			sum += i;
//
//			// System.out.print(i + " ");
//		}
//		return sum;
//	}

	static int sum = 0;

	static int printNumbersRecursive(int i) {
		sum += i; // adds current i to sum
		if (i == 10) { // checks condition - when condition is met, if is executed
			System.out.println("Method over");
			return sum;
		}
		
		i++; // increments i
		return printNumbersRecursive(i); // calls the method once again
	}
}
