package com.sda.codingfundamentals.timoce.main;

public class Application {

	public static boolean checkAge(int age) {

		if (age >= 18) {
			// System.out.println("Is Major");
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {

		boolean isTrue = true;
		int paulsAge = 19;
//      short, byte, int, long
//      long longNumber = 45L;
//      float, double;
		float numberFloat = 45.67f;
		char gender = 'm';
		String myName = new String("Paul");
		String nameSecond = "Trestian";
		int[] arrayWithNumbers = new int[10];
		int[] arrayWithThreeNumbers = { 5, 6, 7 };
//      for (int i=0; i<arrayWithNumbers.length; i++){
//          //System.out.println("Repeat " + i);
//          arrayWithNumbers[i] = i+1;  
//          System.out.println( arrayWithNumbers[i] );
//      }
//      int i =0;
//      while (i<arrayWithNumbers.length) {
//          arrayWithNumbers[i] = i+1;

//          System.out.println( arrayWithNumbers[i] );
//          i++;
////            i = i + 1;
//      }
		int i = 0;
		do {
			arrayWithNumbers[i] = i + 1;
			System.out.println(arrayWithNumbers[i]);
			i++;

		} while (i < arrayWithNumbers.length);
//      if (paulsAge>=18 || gender == 'm') {
//          System.out.println("Major si Barbat");
//      } else {
//          System.out.println("Minor si nu conteaza genul");
//      }
//      Application myApplication = new Application();

//      myApplication.checkAge(45);

		boolean result = checkAge(45);
		if (result) {
			System.out.println("Metoda o zis ca e major");
		}
	}
}
