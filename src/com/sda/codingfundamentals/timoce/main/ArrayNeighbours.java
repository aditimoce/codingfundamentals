package com.sda.codingfundamentals.timoce.main;

import java.util.Scanner;

public class ArrayNeighbours {

	public static void main(String[] args) {
		// Write a program that reads numbers and adds them into an array. Ask user to
		// enter array size. Then ask for numbers until the array is full. Build another
		// array that has the sum of the element and neighbours for each element from
		// the array. If there is no neighbor, consider 0 value on the missing position.
		// Example:
		// Input array: 1 2 3 4 5
		// Sum of Neighbours array: 3 6 9 12 9

		Scanner scanner = new Scanner(System.in);
		System.out.print("How many numbers? ");
		int arraySizeFromInput = scanner.nextInt();

		int[] initialArray = new int[arraySizeFromInput];

		System.out.print("Enter the numbers: ");
		for (int i = 0; i < initialArray.length; i++) {
			initialArray[i] = scanner.nextInt();
		}
		scanner.close();

		int[] newArray = new int[arraySizeFromInput];
		for (int i = 0; i < newArray.length; i++) {
			if (initialArray.length == 1) {
				newArray[i] = initialArray[i];
				break;
			} else if (i == 0) {
				newArray[i] = initialArray[i] + initialArray[i + 1];
			} else if (i == initialArray.length - 1) {
				newArray[i] = initialArray[i] + initialArray[i - 1];
			} else if (i > 0 && i < initialArray.length - 1) {
				newArray[i] = initialArray[i] + initialArray[i + 1] + initialArray[i - 1];
			}

			System.out.print(newArray[i] + " ");
		}

	}
}
