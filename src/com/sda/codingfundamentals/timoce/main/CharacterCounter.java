package com.sda.codingfundamentals.timoce.main;

import java.util.Scanner;

public class CharacterCounter {

	public static void main(String[] args) {
		// Write a program that accepts a sentence and calculate the number of letters,
		// digits and other characters.
		// Suppose the following input is supplied to the program: hello world! 123
		// Then, the output should be:
		// LETTERS 10
		// DIGITS 3
		// OTHER 3

		Scanner scanner = new Scanner(System.in);
		System.out.print("Type your sentence: ");
		String sentenceFromInput = scanner.nextLine();

		String sentence = sentenceFromInput;
		int numberOfLetters = 0;
		int numberOfDigits = 0;
		int numberOfOthers = 0;

		for (int i = 0; i < sentence.length(); i++) {
			if (Character.isLetter(sentence.charAt(i))) {
				numberOfLetters++;
			} else {
				if (Character.isDigit(sentence.charAt(i))) {
					numberOfDigits++;
				} else {
					numberOfOthers++;
				}
			}
		}
//		int numberOfOthers = sentence.length() - (numberOfLetters + numberOfDigits);
		System.out.println("LETTERS " + numberOfLetters);
		System.out.println("DIGITS " + numberOfDigits);
		System.out.println("OTHERS " + numberOfOthers);
		scanner.close();
	}
}
